package mk.iwec.studentDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mk.iwec.database.DB;
import mk.iwec.student.Student;

public class StudentDAO {
	public static List<Student> getAllStudents() {
		List<Student> studentList = new ArrayList<>();		
		try (DB db = new DB("localhost", "5432", "StudentsDemo", "postgres", "075648917");
				Statement st = db.getConn().createStatement();) {
			ResultSet rs = st.executeQuery("Select s.id, s.name , s.surname "
					+ " From student as s ");
			while (rs.next()) {
				studentList.add(new Student(rs.getInt("id"), rs.getString("name"), rs.getString("surname")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		
		} catch (Exception e1) {
			
			e1.printStackTrace();
		}
		return studentList;
	}
}
