package mk.iwec.student;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Student {
	private int iD;
	private String name;
	private String surname;
}
