package mk.iwec.main;

import java.util.List;

import mk.iwec.groups.Shuffle;
import mk.iwec.student.Student;
import mk.iwec.studentDAO.StudentDAO;


public class App {

	public static void main(String[] args) {
		List<Student> lista = StudentDAO.getAllStudents();
		List<Student> prvaGrupa = Shuffle.makeGroup(lista);
		List<Student> vtoraGrupa = Shuffle.makeGroup(lista);
		List<Student> tretaGrupa = Shuffle.makeGroup(lista);
		System.out.println("prva grupa: " + prvaGrupa);
		System.out.println("vtora grupa: " + vtoraGrupa);
		System.out.println("treta grupa" + tretaGrupa);
		
	}

}
