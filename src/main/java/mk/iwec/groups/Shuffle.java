package mk.iwec.groups;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mk.iwec.student.Student;

public class Shuffle {
	public static ArrayList<Student> makeGroup(List<Student> lista) {
		if (lista.size() < 3) {  //if list size as parameter is less than 3 return null
			return null;
		}
		ArrayList<Student> grupa = new ArrayList<>();

		for (int i = 2; i >= 0; i--) { // we iterate the list from index at 2 to 0 and we shuffle for each element
			Collections.shuffle(lista);
			grupa.add(lista.get(i));  // we add the element from the list to group element and the same element is removed
			lista.remove(i);		  // from the list as parameter
		}
		return grupa;					// return group as list with 3 objects inside
	}
}
