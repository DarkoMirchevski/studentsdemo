package mk.iwec.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB implements AutoCloseable {
	private Connection conn;

	public DB(String hostname, String port, String dbName, String username, String pw) {
		String url = "jdbc:postgresql://" + hostname + ':' + port + '/' + dbName;
		try {
			conn = DriverManager.getConnection(url, username, pw);
			if(conn != null) {
				System.out.println("uspesna konekcija");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public Connection getConn() {
		return conn;
	}

	@Override
	public void close() throws Exception {
		if(conn != null) {
			conn.close();
		}

	}
}

